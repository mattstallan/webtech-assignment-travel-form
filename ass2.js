/* javascript for Assignment 2 Forms*/

$( document ).ready(function() {
	
	//changes value of output to value of range slider
	$('#budget').on("change", function() {
		$('.output').val(this.value);
		}).trigger("change");
		
	//shows partner input fields
	$('#has_partner').on("change", function(){
		if($(this).is(':checked')){
			$('#partner').removeClass('hide');
			$("input[name=partner_firstname]").addClass("required").prop('required',true);
			$("input[name=partner_lastname]").addClass("required").prop('required',true);
			$("input[name=partner_gender]").addClass("required").prop('required',true);
			$("input[name=partner_DOB]").addClass("required").prop('required',true);
		}
	});
	//hides partner input fields
	$('#no_partner').on('change', function(){
		if($(this).is(':checked')){
			$('#partner').addClass('hide');
			$("input[name=partner_firstname]").removeClass("required").removeAttr('required');
			$("input[name=partner_lastname]").removeClass("required").removeAttr('required');
			$("input[name=partner_gender]").removeClass("required").removeAttr('required');
			$("input[name=partner_DOB]").removeClass("required").removeAttr('required');
			}
	});
	//hides button to add child info
	$('#no_children').on('change', function(){
		if($(this).is(':checked')){
			$('#children').empty();
			$('#addChildBtn').addClass('hide');
		}
	});	
	//shows button to add child info
	$('#yes_children').on('change', function(){
		if($(this).is(':checked')){
			$('#addChildBtn').removeClass('hide');
		}
	});
	//shows rest day input
	$('#require_rest').on('change', function(){
		if($(this).is(':checked')){
			$('#restD').removeClass("hide");
		}
	});
	//hides rest day input
	$('#no_Rest').on('change', function(){
		if($(this).is(':checked')){
			$('#restD').addClass("hide");
		}
	});	
	
	var i = 1;
	//adds input fields for children and rechecks validation
	$('#addChildBtn').on('click', function(e){
		e.preventDefault();
		$('#children').append('<div><p><label>*Name: <input class="float_right required" type="text" title="Legal Characters: A-Z, a-z, -" required="required" pattern="[A-Za-z -]{2,}" name="child '+i+
		' name"/></label></p><p><label>*Date Of Birth: <input class="float_right required" type="date" required="required" name="child '+i+
		' DOB" /></label></p><label for="child '+i+' gender">Gender: <input type="radio" name="child '+i+
		' gender" value="male" checked="checked" />Male <input type="radio" name="child '+i+
		' gender" value="female"/>Female</label></br><a href="#" class="remove_field">Remove</a></div>');
		i++;
		$('.current select, input').change(function(e) {
			controlgroup = $(e.currentTarget);
			if (!e.target.checkValidity()) {
				controlgroup.addClass('invalid');
			} else {
				controlgroup.removeClass('invalid');
				controlgroup.removeClass('required');
				controlgroup.removeClass('required2');
		}
	});
	});
	// removes input fields for children
	 $('#children').on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); 
		$(this).parent('div').remove(); 
		i--;
    });
	
	//adds textarea if other is selected
	$('#accom').change(function(){
		if($(this).val()== "other"){
			$('#otherAccom').removeClass('hide');
		}else{
			$('#otherAccom').addClass('hide');
		}
	});
	$('#otherTravel').change(function(){
		if($(this).is(':checked')){
			$('#otherTravelText').removeClass('hide');
		}else{
			$('#otherTravelText').addClass('hide');
		}
	});
	$('#prefDest').change(function(){
		if($(this).val()== "other"){
			$('#otherDestText').removeClass('hide');
		}else{
			$('#otherDestText').addClass('hide');
		}
	});
	

	//validates data in inputs on change
	$('.current select, input').change(function(e) {
		controlgroup = $(e.currentTarget);
			if (!e.target.checkValidity()) {
				controlgroup.addClass('invalid');
			} else {
				controlgroup.removeClass('invalid');
				controlgroup.removeClass('required');
				controlgroup.removeClass('required2');
		}
	});
	$("#endHol").change(function(){
		if($("#endHol").val() < $("#startHol").val()){
			$("#endHol").get(0).setCustomValidity("The End Date must Be greater Than Start Date");
		}else{
			$("#endHol").get(0).setCustomValidity('');
		}
	});
	
	
	//next button to move through sections and check that all .required field
	$('.next').click(function() {
		if($(".invalid").length == 0 && $(".required").length == 0) {
			$('.current').removeClass('current').hide().next().show().addClass('current');
		}else{
			$(".required").each(function(){
				$(this).addClass("invalid");
			});
		}
	});
	//next button for second page and validation
	$('.next2').click(function() {
		if($(".invalid").length == 0 && $(".required2").length == 0) {
			$('.current').removeClass('current').hide().next().show().addClass('current');
		}else{
			$(".required2").each(function(){
				$(this).addClass("invalid");
			});
		}
	});
	
	
	
	//To go to previous section
    $('.previous').click(function () {
        $('.current').removeClass('current').hide().prev().show().addClass('current');
    });
	
	
	
});



